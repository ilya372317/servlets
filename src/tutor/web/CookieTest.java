package tutor.web;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CookieTest extends HttpServlet {

    private static final long serialVersionUID = 6733387330018707732L;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	response.setContentType("text/html");
	String name = request.getParameter("username");

	Cookie coockie = new Cookie("username", name);
	coockie.setMaxAge(30 * 60);

	response.addCookie(coockie);

	RequestDispatcher view = request.getRequestDispatcher("cookieresult.jsp");
	view.forward(request, response);
    }

}
