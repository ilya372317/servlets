package tutor.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import tutor.entity.Teacher;

public class TeacherController extends HttpServlet {

	private static final long serialVersionUID = 5992257060647714651L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		ArrayList<Integer> integers = new ArrayList<Integer>();
		integers.add(1);
		integers.add(2);
		integers.add(3);
		session.setAttribute("ints", integers);

		Teacher teacher = new Teacher();
		teacher.setName("Sonya");
		request.setAttribute("teacher", teacher);

		RequestDispatcher view = request.getRequestDispatcher("/test.jsp");
		view.forward(request, response);
	}

}
