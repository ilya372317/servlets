package tutor.web;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CheckCookie extends HttpServlet {

    private static final long serialVersionUID = -5645007420075211955L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();

	Cookie[] cookies = request.getCookies();

	for (Cookie cookie : cookies) {
	    if (cookie.getName().equals("username")) {
		String username = cookie.getValue();
		out.print(username + "</br>");
	    }
	}
    }

}
