package tutor.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class JspTagServlet extends HttpServlet {

    private static final long serialVersionUID = 3600376353166038597L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	List<String> testForLoop = new ArrayList<String>();
	testForLoop.add("costume string one");
	testForLoop.add("costume string second");
	testForLoop.add("costume string third");
	
	response.setContentType("text/html");
	request.setAttribute("loop", testForLoop);
	
	RequestDispatcher view = request.getRequestDispatcher("/tag.jsp");
	view.forward(request, response);
    }

}
