package tutor.web.jsp;

import java.io.IOException;

import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

public class SimpleTagTest extends SimpleTagSupport {
		
	@Override
	public void doTag() throws JspException, IOException {
		this.getJspContext().getOut().print("is simple way to use costume tag");
	}

}
