package tutor.web.jsp;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

public class LoopListTag extends SimpleTagSupport {
    
    private List<String> stringList;

    @Override
    public void doTag() throws IOException, JspException {

	for (String item : this.stringList) {
	    this.getJspContext().setAttribute("item", item);
	    this.getJspBody().invoke(null);
	}
    }
    
    public void setList(List<String> list) {
	this.stringList = list;
    }

}
