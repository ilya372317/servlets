package tutor.entity;

public class Teacher {
	
	private String name;
	
	public Teacher() {
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

}
